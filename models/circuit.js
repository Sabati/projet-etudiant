/*
* config.Db contient les parametres de connection à la base de données
* il va créer aussi un pool de connexions utilisables
* sa méthode getConnection permet de se connecter à MySQL
*
*/
var db = require('../configDb');

/*
*
*/

module.exports.getListeCircuit = function (callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT cirnum, payadrdrap, cirnom FROM circuit c INNER JOIN pays p ";
						sql= sql + "ON p.paynum=c.paynum ORDER BY cirnom";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};


module.exports.getDetailCircuit = function (circuitNum, callback) {
   // connection à la base
	db.getConnection(function(err, connexion){
        if(!err){
        	  // s'il n'y a pas d'erreur de connexion
        	  // execution de la requête SQL
						var sql ="SELECT cirnum, cirnom, paynom, cirlongueur, cirnbspectateurs, ciradresseimage, cirtext";
						sql = sql + " FROM circuit c INNER JOIN pays p ";
						sql= sql + "ON p.paynum=c.paynum WHERE cirnum ="+circuitNum+" ";
						//console.log (sql);
            connexion.query(sql, callback);

            // la connexion retourne dans le pool
            connexion.release();
         }
      });
};
